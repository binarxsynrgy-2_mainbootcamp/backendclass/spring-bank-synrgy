package com.bank.synrgy.main.module.account.transfers.presenter;

import com.bank.synrgy.main.module.account.transfers.model.dto.DetailAccountTransfer;
import com.bank.synrgy.main.module.account.transfers.service.AccountTransferServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/v1/account")
public class AccountTransferPresenter {

    @Autowired
    AccountTransferServiceImpl accountTransferService;

    @PostMapping("/transfers")
    public Map<String, Object> transferAccountBalance(
            @RequestBody DetailAccountTransfer detailAccountTransferPayload) {
        Map<String, Object> map = new HashMap<>();
        DetailAccountTransfer detailAccountTransfer = accountTransferService.detailAccountTransfer(detailAccountTransferPayload);

        map.put("data", detailAccountTransfer);
        return map;
    }
}
