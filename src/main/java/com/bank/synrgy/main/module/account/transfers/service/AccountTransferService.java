package com.bank.synrgy.main.module.account.transfers.service;

import com.bank.synrgy.main.module.account.transfers.model.dto.DetailAccountTransfer;

public interface AccountTransferService {
    DetailAccountTransfer detailAccountTransfer(DetailAccountTransfer detailAccountTransfer);
}
