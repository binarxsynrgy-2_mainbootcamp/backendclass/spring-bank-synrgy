package com.bank.synrgy.main.module.account.payments.service;

import com.bank.synrgy.main.module.account.balances.model.AccountBalance;
import com.bank.synrgy.main.module.account.histories.model.AccountHistory;
import com.bank.synrgy.main.module.account.payments.dto.DetailPaymentAccount;
import com.bank.synrgy.main.module.account.balances.repository.AccountBalanceRepository;
import com.bank.synrgy.main.module.account.histories.repository.AccountHistoryRepository;
import com.bank.synrgy.main.utility.ModelMapperUtil;
import com.bank.synrgy.main.utility.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class AccountPaymentServiceImpl implements AccountPaymentService {
    @Autowired
    AccountBalanceRepository accountBalanceRepository;

    @Autowired
    AccountHistoryRepository accountHistoryRepository;

    @Autowired
    ModelMapperUtil modelMapperUtil;

    @Override
    public DetailPaymentAccount paymentAccount(DetailPaymentAccount detailPaymentAccountPayload) {
        AccountBalance accountBalance = accountBalanceRepository
                .findTopByAccountNumber(detailPaymentAccountPayload.getAccountNumber())
                .orElseThrow(() -> new ResourceNotFoundException("User balanace not found with " +
                        detailPaymentAccountPayload.getAccountNumber()));
        DetailPaymentAccount detailPaymentAccount = mapperPaymentAccount(accountBalance, detailPaymentAccountPayload);
        return detailPaymentAccount;
    }

    private DetailPaymentAccount mapperPaymentAccount(AccountBalance accountBalance,
                                                      DetailPaymentAccount detailPaymentAccountPayload) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(new Date());

        AccountHistory detailPaymentAccount = modelMapperUtil
                .modelMapper()
                .map(detailPaymentAccountPayload, AccountHistory.class);

        accountBalance.setAccountBalance(accountBalance.getAccountBalance() -
                detailPaymentAccountPayload.getNominee());

        detailPaymentAccount.setAccountNumber(detailPaymentAccountPayload.getAccountNumber());
        detailPaymentAccount.setNominee(detailPaymentAccountPayload.getNominee());
        detailPaymentAccount.setTransactionType(detailPaymentAccountPayload.getTransactionType());
        detailPaymentAccount.setTransactionDate(date);
        accountHistoryRepository.save(detailPaymentAccount);

        return detailPaymentAccountPayload;
    }
}
