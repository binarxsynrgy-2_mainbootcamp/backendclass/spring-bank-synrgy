package com.bank.synrgy.main.module.account.balances.repository;

import com.bank.synrgy.main.module.account.balances.model.AccountBalance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountBalanceRepository extends JpaRepository<AccountBalance, Long> {
    Optional<AccountBalance> findTopByAccountNumber(String accountNumber);
}
