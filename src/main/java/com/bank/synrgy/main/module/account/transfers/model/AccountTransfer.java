package com.bank.synrgy.main.module.account.transfers.model;

import com.bank.synrgy.main.utility.AuditModel;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "account_transfers")
@Data
public class AccountTransfer extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "transfer_from")
    @JsonProperty("transfer_from")
    private String transferFrom;

    @Column(name = "transfer_to")
    @JsonProperty("transfer_to")
    private String transferTo;

    @Column(name = "nominee")
    private Integer nominee;

    @Column(name = "transfer_date")
    @JsonProperty("transfer_date")
    private Date transferDate;
}
