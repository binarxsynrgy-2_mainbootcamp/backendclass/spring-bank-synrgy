package com.bank.synrgy.main.module.account.histories.service;

import com.bank.synrgy.main.module.account.histories.model.dto.ListAccountHistories;

import java.util.List;

public interface AccountHistoriesService {
    List<ListAccountHistories> listAccountHistories(String accountNumber, String transactionDate);
}
