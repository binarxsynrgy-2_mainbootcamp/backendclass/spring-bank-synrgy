package com.bank.synrgy.main.module.auth.service;

import com.bank.synrgy.main.module.auth.model.dto.CreateUserRegistration;

public interface UserService {
    CreateUserRegistration userRegistration(CreateUserRegistration createUserRegistrationPayload);
}
