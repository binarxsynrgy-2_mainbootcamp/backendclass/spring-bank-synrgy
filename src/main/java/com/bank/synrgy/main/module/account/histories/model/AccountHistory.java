package com.bank.synrgy.main.module.account.histories.model;

import com.bank.synrgy.main.utility.AuditModel;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "account_histories")
public class AccountHistory extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account_number")
    @JsonProperty("account_number")
    private String accountNumber;

    @Column(name = "transaction_type")
    @JsonProperty("transaction_type")
    private String transactionType;

    @Column(name = "nominee")
    @JsonProperty("nominee")
    private Integer nominee;

    @Column(name = "transaction_date")
    @JsonProperty("transaction_date")
    private String transactionDate;
}
