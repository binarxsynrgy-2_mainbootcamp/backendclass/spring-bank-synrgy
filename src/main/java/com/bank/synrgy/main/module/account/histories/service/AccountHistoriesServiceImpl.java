package com.bank.synrgy.main.module.account.histories.service;

import com.bank.synrgy.main.module.account.histories.model.AccountHistory;
import com.bank.synrgy.main.module.account.histories.model.dto.ListAccountHistories;
import com.bank.synrgy.main.module.account.histories.repository.AccountHistoryRepository;
import com.bank.synrgy.main.utility.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountHistoriesServiceImpl implements AccountHistoriesService {
    @Autowired
    AccountHistoryRepository accountHistoryRepository;

    @Autowired
    ModelMapperUtil modelMapperUtil;

    @Override
    public List<ListAccountHistories> listAccountHistories(String accountNumber, String transactionDate) {
        List<AccountHistory> accountHistoryList = accountHistoryRepository
                .findByAccountNumberAndTransactionDate(accountNumber, transactionDate);
        List<ListAccountHistories> resultAccountHistories = mapperAccountHistoriesToListAccountHistoriesDto(accountHistoryList);

        return resultAccountHistories;
    }

    private List<ListAccountHistories> mapperAccountHistoriesToListAccountHistoriesDto(
            List<AccountHistory> accountHistoryList) {
        List<ListAccountHistories> listAccountHistoriesDtos = new ArrayList<>();

        for (AccountHistory data: accountHistoryList) {
            ListAccountHistories listAccountHistories = modelMapperUtil
                    .modelMapper()
                    .map(accountHistoryList, ListAccountHistories.class);

            listAccountHistories.setAccountNumber(data.getAccountNumber());
            listAccountHistories.setTransactionType(data.getTransactionType());
            listAccountHistories.setNominee(data.getNominee());
            listAccountHistories.setTransactionDate(data.getTransactionDate());

            listAccountHistoriesDtos.add(listAccountHistories);
        }

        return listAccountHistoriesDtos;
    }
}
