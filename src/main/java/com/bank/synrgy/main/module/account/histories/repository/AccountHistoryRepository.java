package com.bank.synrgy.main.module.account.histories.repository;

import com.bank.synrgy.main.module.account.histories.model.AccountHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountHistoryRepository extends JpaRepository<AccountHistory, Long> {
    List<AccountHistory> findByAccountNumberAndTransactionDate(String accountNumber, String transactionDate);
}
