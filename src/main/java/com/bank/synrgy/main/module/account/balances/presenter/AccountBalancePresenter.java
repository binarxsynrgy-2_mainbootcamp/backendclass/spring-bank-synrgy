package com.bank.synrgy.main.module.account.balances.presenter;

import com.bank.synrgy.main.module.account.balances.model.dto.DetailAccountBalance;
import com.bank.synrgy.main.module.account.balances.service.AccountBalanceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/v1/account")
public class AccountBalancePresenter {

    @Autowired
    AccountBalanceServiceImpl accountBalanceService;

    @GetMapping("/balances")
    public Map<String, Object> getUserAccountBalance(@RequestParam String accountNumber) {
        Map<String, Object> map = new HashMap<>();
        DetailAccountBalance detailAccountBalance = accountBalanceService.detailUserBalance(accountNumber);

        map.put("data", detailAccountBalance);
        return map;
    }
}
