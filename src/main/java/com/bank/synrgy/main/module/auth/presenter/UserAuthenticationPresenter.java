package com.bank.synrgy.main.module.auth.presenter;

import com.bank.synrgy.main.module.auth.model.LoginRequest;
import com.bank.synrgy.main.module.auth.model.dto.CreateUserRegistration;
import com.bank.synrgy.main.module.auth.service.JwtUserDetailService;
import com.bank.synrgy.main.module.auth.service.UserServiceImpl;
import com.bank.synrgy.main.utility.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/v1/auth")
public class UserAuthenticationPresenter {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    JwtUserDetailService userDetailsService;

    @PostMapping("/signup")
    public Map<String, Object> createUserRegistration(
            @RequestBody CreateUserRegistration createUserRegistrationPayload) {
        Map<String, Object> map = new HashMap<>();
        CreateUserRegistration createUserRegistration = userService.userRegistration(createUserRegistrationPayload);

        map.put("message", "User registration successfully");
        return map;
    }

    @PostMapping("/login")
    public Map<String, Object> loginUserAuthentication(@RequestBody LoginRequest authenticationRequest) throws Exception {
        Map<String, Object> map = new HashMap<>();

        authenticate(authenticationRequest.getPhoneNumber(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getPhoneNumber());

        final String token = jwtTokenUtil.generateToken(userDetails);

        map.put("access_token", token);
        return map;
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
