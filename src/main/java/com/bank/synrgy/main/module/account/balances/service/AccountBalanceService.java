package com.bank.synrgy.main.module.account.balances.service;

import com.bank.synrgy.main.module.account.balances.model.dto.DetailAccountBalance;

public interface AccountBalanceService {
    DetailAccountBalance detailUserBalance(String accountNumber);
}
