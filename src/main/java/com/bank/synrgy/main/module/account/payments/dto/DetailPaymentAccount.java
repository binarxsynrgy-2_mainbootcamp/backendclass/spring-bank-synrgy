package com.bank.synrgy.main.module.account.payments.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class DetailPaymentAccount {
    @JsonProperty("account_number")
    private String accountNumber;

    @JsonProperty("transaction_type")
    private String transactionType;

    @JsonProperty("nominee")
    private Integer nominee;

    @JsonProperty("transaction_date")
    private Date transactionDate;
}
