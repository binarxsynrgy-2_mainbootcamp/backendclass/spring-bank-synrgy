package com.bank.synrgy.main.module.account.histories.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ListAccountHistories {
    @JsonProperty("account_number")
    private String accountNumber;

    @JsonProperty("nominee")
    private Integer nominee;

    @JsonProperty("transaction_type")
    private String transactionType;

    @JsonProperty("transaction_date")
    private String transactionDate;
}
