package com.bank.synrgy.main.module.account.transfers.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class DetailAccountTransfer {
    @JsonProperty("transfer_from")
    private String transferFrom;

    @JsonProperty("transfer_to")
    private String transferTo;

    @JsonProperty("transfer_date")
    private Date transferDate;

    @JsonProperty("transfer_nominee")
    private Integer transferNominee;
}
