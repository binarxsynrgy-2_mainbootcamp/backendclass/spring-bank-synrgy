package com.bank.synrgy.main.module.auth.service;

import com.bank.synrgy.main.module.account.balances.model.AccountBalance;
import com.bank.synrgy.main.module.auth.model.Users;
import com.bank.synrgy.main.module.auth.model.dto.CreateUserRegistration;
import com.bank.synrgy.main.module.account.balances.repository.AccountBalanceRepository;
import com.bank.synrgy.main.module.auth.repository.UserRepository;
import com.bank.synrgy.main.utility.PasswordEncoderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountBalanceRepository accountBalanceRepository;

    @Autowired
    PasswordEncoderUtil passwordEncoderUtil;

    @Override
    public CreateUserRegistration userRegistration(CreateUserRegistration createUserRegistrationPayload) {
        Users users = new Users();
        users.setPhoneNumber(createUserRegistrationPayload.getPhoneNumber());
        users.setPassword(passwordEncoderUtil.encoder().encode(createUserRegistrationPayload.getPassword()));

        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setAccountNumber(users.getPhoneNumber());
        accountBalance.setAccountBalance(1000000000);

        userRepository.save(users);
        accountBalanceRepository.save(accountBalance);

        return createUserRegistrationPayload;
    }
}
