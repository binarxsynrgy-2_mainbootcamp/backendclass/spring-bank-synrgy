package com.bank.synrgy.main.module.auth.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class LoginRequest implements Serializable {
    @JsonProperty("phone_number")
    private String phoneNumber;
    private String password;
}
