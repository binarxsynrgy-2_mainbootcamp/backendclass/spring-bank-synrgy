package com.bank.synrgy.main.module.auth.service;

import com.bank.synrgy.main.module.auth.model.Users;
import com.bank.synrgy.main.module.auth.repository.UserRepository;
import com.bank.synrgy.main.utility.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class JwtUserDetailService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        Users user = userRepository
                .findByPhoneNumber(phoneNumber)
                .orElseThrow(() -> new ResourceNotFoundException("Account not found : " + phoneNumber));

        if (user.getPhoneNumber().equals(phoneNumber)) {
            return new User(user.getPhoneNumber(), user.getPassword(),
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("Account not found : " + phoneNumber);
        }
    }
}
