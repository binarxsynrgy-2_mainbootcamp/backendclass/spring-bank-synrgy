package com.bank.synrgy.main.module.account.transfers.repository;

import com.bank.synrgy.main.module.account.transfers.model.AccountTransfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountTransferRepository extends JpaRepository<AccountTransfer, Long> {
}
