package com.bank.synrgy.main.module.auth.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreateUserRegistration {
    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("password")
    private String password;
}
