package com.bank.synrgy.main.module.account.balances.model;

import com.bank.synrgy.main.utility.AuditModel;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "account_balances")
@Data
public class AccountBalance extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account_number")
    @JsonProperty("account_number")
    private String accountNumber;

    @Column(name = "account_balance")
    @JsonProperty("account_balance")
    private Integer accountBalance;
}
