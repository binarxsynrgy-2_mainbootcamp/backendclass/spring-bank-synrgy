package com.bank.synrgy.main.module.account.balances.service;

import com.bank.synrgy.main.module.account.balances.model.AccountBalance;
import com.bank.synrgy.main.module.account.balances.model.dto.DetailAccountBalance;
import com.bank.synrgy.main.module.account.balances.repository.AccountBalanceRepository;
import com.bank.synrgy.main.module.auth.repository.UserRepository;
import com.bank.synrgy.main.utility.ModelMapperUtil;
import com.bank.synrgy.main.utility.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountBalanceServiceImpl implements AccountBalanceService {

    @Autowired
    AccountBalanceRepository accountBalanceRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModelMapperUtil modelMapperUtility;

    @Override
    public DetailAccountBalance detailUserBalance(String accountNumber) {
        Optional<AccountBalance> accountBalance = Optional.ofNullable(accountBalanceRepository.findTopByAccountNumber(accountNumber)
                .orElseThrow(() -> new ResourceNotFoundException("User balance not found with account " + accountNumber)));
        DetailAccountBalance detailAccountBalance = mapperDetailUserBalance(accountBalance);

        return detailAccountBalance;
    }

    private DetailAccountBalance mapperDetailUserBalance(Optional<AccountBalance> accountBalance) {
        DetailAccountBalance detailAccountBalance = modelMapperUtility
                .modelMapper()
                .map(accountBalance, DetailAccountBalance.class);

        accountBalance.ifPresent(data -> {
            detailAccountBalance.setId(data.getId());
            detailAccountBalance.setAccountNumber(data.getAccountNumber());
            detailAccountBalance.setAccountBalance(data.getAccountBalance());
        });

        return detailAccountBalance;
    }
}
