package com.bank.synrgy.main.module.account.histories.presenter;

import com.bank.synrgy.main.module.account.histories.model.dto.ListAccountHistories;
import com.bank.synrgy.main.module.account.histories.service.AccountHistoriesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/account")
public class AccountHistoriesPresenter {
    @Autowired
    AccountHistoriesServiceImpl accountHistoriesService;

    @GetMapping("/mutations")
    public Map<String, Object> getAccountHistories(
            @RequestParam(name = "accountNumber") String accountNumber,
            @RequestParam(name = "transactionDate") String transactionDate) {
        Map<String, Object> map = new HashMap<>();

        List<ListAccountHistories> listAccountHistories = accountHistoriesService
                .listAccountHistories(accountNumber, transactionDate);

        map.put("data", listAccountHistories);
        return map;

    }
}
