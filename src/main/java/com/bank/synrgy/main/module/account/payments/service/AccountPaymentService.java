package com.bank.synrgy.main.module.account.payments.service;

import com.bank.synrgy.main.module.account.payments.dto.DetailPaymentAccount;

public interface AccountPaymentService {
    DetailPaymentAccount paymentAccount(DetailPaymentAccount detailPaymentAccountPayload);
}
