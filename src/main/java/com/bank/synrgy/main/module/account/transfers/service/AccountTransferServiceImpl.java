package com.bank.synrgy.main.module.account.transfers.service;

import com.bank.synrgy.main.module.account.balances.model.AccountBalance;
import com.bank.synrgy.main.module.account.histories.model.AccountHistory;
import com.bank.synrgy.main.module.account.transfers.model.AccountTransfer;
import com.bank.synrgy.main.module.account.transfers.model.dto.DetailAccountTransfer;
import com.bank.synrgy.main.module.account.balances.repository.AccountBalanceRepository;
import com.bank.synrgy.main.module.account.histories.repository.AccountHistoryRepository;
import com.bank.synrgy.main.module.account.transfers.repository.AccountTransferRepository;
import com.bank.synrgy.main.utility.ModelMapperUtil;
import com.bank.synrgy.main.utility.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class AccountTransferServiceImpl implements AccountTransferService {
    @Autowired
    AccountBalanceRepository accountBalanceRepository;

    @Autowired
    AccountTransferRepository accountTransferRepository;

    @Autowired
    AccountHistoryRepository accountHistoryRepository;

    @Autowired
    ModelMapperUtil modelMapperUtility;

    @Override
    public DetailAccountTransfer detailAccountTransfer(DetailAccountTransfer detailAccountTransferPayload) {
        DetailAccountTransfer detailAccountTransfer = mapperAccountTransfer(detailAccountTransferPayload);
        return detailAccountTransfer;
    }

    private DetailAccountTransfer mapperAccountTransfer(DetailAccountTransfer detailAccountTransferPayload) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(new Date());

        AccountTransfer accountTransfer = new AccountTransfer();
        accountTransfer.setTransferFrom(detailAccountTransferPayload.getTransferFrom());
        accountTransfer.setTransferTo(detailAccountTransferPayload.getTransferTo());
        accountTransfer.setNominee(detailAccountTransferPayload.getTransferNominee());
        accountTransfer.setTransferDate(new Date());

        AccountHistory accountHistory = new AccountHistory();
        accountHistory.setAccountNumber(detailAccountTransferPayload.getTransferFrom());
        accountHistory.setTransactionType("TRANSFER");
        accountHistory.setNominee(detailAccountTransferPayload.getTransferNominee());
        accountHistory.setTransactionDate(date);

        // Divide balances
        AccountBalance accountBalanceTransferFrom = accountBalanceRepository
        .findTopByAccountNumber(detailAccountTransferPayload.getTransferFrom())
        .orElseThrow(() -> new ResourceNotFoundException("User balance not found with"
                + detailAccountTransferPayload.getTransferFrom()));

        // Add balances
        AccountBalance accountBalanceTransferTo = accountBalanceRepository
                .findTopByAccountNumber(detailAccountTransferPayload.getTransferTo())
                .orElseThrow(() -> new ResourceNotFoundException("User balance not found with"
                        + detailAccountTransferPayload.getTransferTo()));

        // Divide balances process
        accountBalanceTransferTo.setAccountBalance(accountBalanceTransferTo.getAccountBalance() + accountTransfer.getNominee());
        accountBalanceTransferFrom.setAccountBalance(accountBalanceTransferFrom.getAccountBalance() - accountTransfer.getNominee());
        accountTransferRepository.save(accountTransfer);

        // Saved on histories transaction
        accountHistoryRepository.save(accountHistory);

        return detailAccountTransferPayload;
    }

}
