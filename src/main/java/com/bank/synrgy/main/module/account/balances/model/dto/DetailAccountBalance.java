package com.bank.synrgy.main.module.account.balances.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DetailAccountBalance {
    private Long id;

    @JsonProperty("account_number")
    private String accountNumber;

    @JsonProperty("account_balance")
    private Integer accountBalance;
}
