package com.bank.synrgy.main.module.account.payments.presenter;

import com.bank.synrgy.main.module.account.payments.dto.DetailPaymentAccount;
import com.bank.synrgy.main.module.account.payments.service.AccountPaymentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/v1/account")
public class AccountPaymentPresenter {
    @Autowired
    AccountPaymentServiceImpl accountPaymentService;

    @PostMapping("/payments")
    public Map<String, Object> paymentAccountTransfer(
            @RequestBody DetailPaymentAccount detailPaymentAccountPayload) {
        Map<String, Object> map = new HashMap<>();
        DetailPaymentAccount detailPaymentAccount = accountPaymentService.paymentAccount(detailPaymentAccountPayload);

        map.put("message", "Payment successfully");
        map.put("data", detailPaymentAccount);
        return map;
    }
}
